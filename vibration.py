from qt import *


class Vibration(BaseWindow):
    def __init__(self, *args):
        super().__init__(*args)

        hlay = QHBoxLayout(self)

        lay = QVBoxLayout()
        hlay.addLayout(lay)

        hlay.addStretch()

        lay.addWidget(self.menu)

        border = BorderedFrame()
        lay.addWidget(border)

        lay.addStretch()

        form = QFormLayout(border)

        self.k_text = QLineEdit()
        form.addRow("K Factor", self.k_text)

        self.b_text = QLineEdit()
        form.addRow("B Factor", self.b_text)

        self.mic_text = UnitLineEdit("lb")
        form.addRow("Maximum explosive per delay", self.mic_text)

        self.dist_text = UnitLineEdit("feet", 15)
        form.addRow("Distance from blast source", self.dist_text)

        form.addRow("", QWidget())

        ppv = QPushButton("Calculate in PPV")
        ppv.clicked.connect(self.ppv)

        self.ppv_text = QLineEdit()
        self.ppv_unit = QLabel("in/sec")

        rhlay = QHBoxLayout()
        rhlay.addWidget(self.ppv_text)
        rhlay.addWidget(self.ppv_unit)
        form.addRow(ppv, rhlay)

        reset = QPushButton("Reset")
        reset.clicked.connect(self.reset)

        form.addRow(reset, QWidget())



    def ppv(self):
        k = float(self.k_text.text())
        try:
            k_float = float(k)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'k constant value missing')
        b = float(self.b_text.text())
        try:
            b_float = float(b)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'b constant value missing')
        q = float(self.mic_text.value())
        try:
            q_float = float(q)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'Maximum instataneous charge?')
        d = float(self.dist_text.value())
        try:
            d1_float = float(d)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'Input distance from blast area')
        ppv = k * (((q ** (0.6667)/d))**b)
        self.ppv_text.setText(f'{ppv: .4f}')
        ...

    def reset(self):
        return (self.k_text.setText(" "),
                self.b_text.setText(" "),
                self.mic_text.cls(),
                self.dist_text.cls(),
                self.ppv_text.setText(" ")
                )
        ...
