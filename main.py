from home import *
from pattern import *
from vibration import *
from manual import *
from report import *


class Drawer(QFrame):
    def __init__(self, main_window: "MainWindow"):
        super().__init__()
        self.main_window = main_window
        self.setMinimumWidth(40)
        self.setMaximumWidth(40)

        self.min = 0
        self.max = 200
        self.animg = QParallelAnimationGroup(self)

        self._anim = []
        for prop in [b"minimumWidth", b"maximumWidth"]:
            anim = QPropertyAnimation(self, prop)
            anim.setDuration(500)
            anim.setEasingCurve(QEasingCurve.InOutQuart)
            self.animg.addAnimation(anim)
            self._anim.append(anim)

        self.expanded = 0
        self.setMinimumWidth(self.min)
        self.setMaximumWidth(self.min)

        lay = QVBoxLayout(self)
        lay.setSpacing(5)

        self.l_k_blast = CenterLabel("L-k Blast")
        self.l_k_blast.setObjectName("l_k_blast")
        lay.addWidget(self.l_k_blast)
        lay.setContentsMargins(5, 5, 0, 5)

        btn_lay = QVBoxLayout()
        lay.addLayout(btn_lay)
        btn_lay.setContentsMargins(10, 0, 0, 0)
        btn_lay.setSpacing(50)

        home = DrawerButton("Home", "home")
        home.clicked.connect(main_window.setCurrentWindow)
        btn_lay.addWidget(home)

        blast_pattern = DrawerButton("Blast Pattern", "explosion", 1)
        blast_pattern.clicked.connect(main_window.setCurrentWindow)
        btn_lay.addWidget(blast_pattern)

        ground_vibration = DrawerButton("Ground Vibration", "vibration", 2)
        ground_vibration.clicked.connect(main_window.setCurrentWindow)
        btn_lay.addWidget(ground_vibration)

        blast_report = DrawerButton("Blast Report", "admin", 3)
        blast_report.clicked.connect(main_window.setCurrentWindow)
        btn_lay.addWidget(blast_report)

        user_manual = DrawerButton("User Manual", "book", 4)
        user_manual.clicked.connect(main_window.setCurrentWindow)
        btn_lay.addWidget(user_manual)

        #about = DrawerButton("About", "info")
        #about.clicked.connect(main_window.setCurrentWindow)
        #btn_lay.addWidget(about)

        lay.addStretch()

    def toggle_drawer(self, toggled: bool):
        for anim in self._anim:
            anim.setStartValue(self.max if self.expanded else self.min)
            anim.setEndValue(self.min if self.expanded else self.max)
        self.animg.start()
        self.expanded = not self.expanded


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()

        lay = QHBoxLayout(self)
        lay.setSpacing(0)
        lay.setContentsMargins(0, 0, 0, 0)

        self.drawer = Drawer(self)
        lay.addWidget(self.drawer)

        self.stack = QStackedLayout()
        lay.addLayout(self.stack)

        self.home = Home(self)
        self.stack.addWidget(self.home)

        self.pattern = Pattern(self)
        self.stack.addWidget(self.pattern)

        self.vibration = Vibration(self)
        self.stack.addWidget(self.vibration)

        self.report = Report(self)
        self.stack.addWidget(self.report)

        self.manual = Manual(self)
        self.stack.addWidget(self.manual)

        # self.stack.setCurrentIndex(1)

        self.move(50, 30)

    def setCurrentWindow(self):
        drawerButton = self.sender()
        self.stack.setCurrentIndex(drawerButton.index)

    # def showEvent(self, a0: QShowEvent) -> None:
    #     self.drawer.toggle_drawer(True) 

class App(QApplication):
    def __init__(self):
        super().__init__([])

        self.main_window = MainWindow()
        # self.main_window.setWindowFlag(Qt.WindowType.WindowStaysOnTopHint)
        self.main_window.destroyed.connect(self.quit)

        self.setStyleSheet(QSS)
        self.main_window.show()

app = App()
sys.exit(app.exec())
