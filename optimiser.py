import pandas as pd
import tensorflow as tf
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, r2_score
from sklearn.neural_network import MLPRegressor


__all__ = ['predict_blast']

#datasets
dataset = pd.read_excel('lang.xlsx')
x = dataset.iloc[:,:-1].values
y = dataset.iloc[:,-1].values
#print (x)
#print (y)
#splitting dataset into training and testing
x_train,x_test,y_train,y_test = train_test_split(x,y,test_size = 0.2, random_state = 0)
#feature scaling
sc = StandardScaler()
x_train = sc.fit_transform(x_train)
x_test = sc.transform(x_test)
#initialising
ann = tf.keras.models.Sequential()
#adding layers
ann.add(tf.keras.layers.Dense(units = 10, activation = 'relu'))
ann.add(tf.keras.layers.Dense(units = 5, activation =  'relu'))
ann.add(tf.keras.layers.Dense(units = 1, activation = 'sigmoid'))
#training the ann
ann.compile(optimizer= 'adam', loss = 'mean_squared_error')
ann.fit(x_train, y_train, batch_size = 32, epochs = 500)

#predict ann
y_predict = ann.predict(x_test)
np.set_printoptions(precision = 2)

#predict blast
ann.predict(sc.transform([[100,10.15,45,58,32]]))
predict_blast = lambda values: ann.predict(sc.transform([values]))
# print(predict_blast)


