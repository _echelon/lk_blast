from qt import *
import math


class Manual(BaseWindow):
    def __init__(self, *args):
        super().__init__(*args)

        vlay1 = QVBoxLayout(self)

        hlay = QHBoxLayout()
        vlay1.addLayout(hlay)

        hlay.addWidget(self.menu)

        input = CenterLabel("Input")
        input.setObjectName("manual_input")
        hlay.addWidget(input)

        hlay1 = QHBoxLayout()
        vlay1.addLayout(hlay1)

        vlay1.addStretch()

        parameter = BorderedFrame()
        parameter.setMaximumWidth(220)

        hlay1.addWidget(parameter)

        p_form = QFormLayout(parameter)
        p_form.setLabelAlignment(Qt.AlignRight)

        self.rock_density_text = UnitLineEdit("g/cc", 20)
        p_form.addRow("Rock density: ", self.rock_density_text)

        self.hole_diameter_text = MeterEdit()
        p_form.addRow("Hole diameter: ", self.hole_diameter_text)

        self.bench_height_text = MeterEdit()
        p_form.addRow("Bench height: ", self.bench_height_text)

        self.ucs_text = UnitLineEdit("MPa", 20)
        p_form.addRow("UCS: ", self.ucs_text)

        self.rqd_text = UnitLineEdit("%")
        p_form.addRow("RQD: ", self.rqd_text)

        self.n_text = QLineEdit()
        p_form.addRow("No of holes: ", self.n_text)

        self.row_text = QLineEdit()
        p_form.addRow("No of holes per row: ", self.row_text)

        self.subdrill_text = MeterEdit()
        p_form.addRow("SUbdrill: ", self.subdrill_text)

        self.hole_depth_text = MeterEdit()
        p_form.addRow("Hole Depth: ", self.hole_depth_text)

        self.stemming_text = MeterEdit()
        p_form.addRow("Stemming: ", self.stemming_text)

        self.fault_text = QLineEdit()
        p_form.addRow("Fault D-H Factor: ", self.fault_text)

        self.burden_text = MeterEdit()
        p_form.addRow("Burden: ", self.burden_text)

        self.spacing_text = MeterEdit()
        p_form.addRow("Spacing: ", self.spacing_text)

        # right input
        vlay2 = QVBoxLayout()
        hlay1.addLayout(vlay2)

        explosive = BorderedFrame()
        vlay2.addWidget(explosive)

        self.bot_charge = QLabel("Bottom charge")
        self.col_charge = QLabel("Column charge")
        self.type = QLabel("Type")
        self.height1 = QLabel("Height")
        self.weight = QLabel("Weight")
        self.density = QLabel("Density")
        self.diameter = QLabel("Diameter")
        self.cart_hole = QLabel("No of cartridge per hole")
        self.cart_box = QLabel("No of cartridge per box")
        self.type_b = QLineEdit()
        self.type_c = QLineEdit()
        self.height_b = QLineEdit()
        self.height_c = QLineEdit()
        self.weight_b = QLineEdit()
        self.weight_c = QLineEdit()
        self.density_b = QLineEdit()
        self.density_c = QLineEdit()
        self.diameter_b = QLineEdit()
        self.diameter_c = QLineEdit()
        self.cart_hole_b = QLineEdit()
        self.cart_hole_c = QLineEdit()
        self.cart_box_b = QLineEdit()
        self.cart_box_c = QLineEdit()
        self.height_unt = QLabel("m")
        self.weight_unit = QLabel("kg")
        self.density_unit = QLabel("g/cc")
        self.diameter_unit = QLabel("m")

        grid2 = QGridLayout(explosive)
        grid2.addWidget(self.bot_charge, 0, 1)
        grid2.addWidget(self.col_charge, 0, 2)
        grid2.addWidget(self.type, 1, 0)
        grid2.addWidget(self.height1, 2, 0)
        grid2.addWidget(self.weight, 3, 0)
        grid2.addWidget(self.density, 4, 0)
        grid2.addWidget(self.diameter, 5, 0)
        grid2.addWidget(self.cart_hole, 6, 0)
        grid2.addWidget(self.cart_box, 7, 0)
        grid2.addWidget(self.type_b, 1, 1)
        grid2.addWidget(self.type_c, 1, 2)
        grid2.addWidget(self.height_b, 2, 1)
        grid2.addWidget(self.height_c, 2, 2)
        grid2.addWidget(self.weight_b, 3, 1)
        grid2.addWidget(self.weight_c, 3, 2)
        grid2.addWidget(self.density_b, 4, 1)
        grid2.addWidget(self.density_c, 4, 2)
        grid2.addWidget(self.diameter_b, 5, 1)
        grid2.addWidget(self.diameter_c, 5, 2)
        grid2.addWidget(self.cart_hole_b, 6, 1)
        grid2.addWidget(self.cart_hole_c, 6, 2)
        grid2.addWidget(self.cart_box_b, 7, 1)
        grid2.addWidget(self.cart_box_c, 7, 2)
        grid2.addWidget(self.height_unt, 2, 3)
        grid2.addWidget(self.weight_unit, 3, 3)
        grid2.addWidget(self.density_unit, 4, 3)
        grid2.addWidget(self.diameter_unit, 5, 3)

        grid2.addItem(QSpacerItem(0, 30))

        output = CenterLabel("Output")
        output.setObjectName("manual_output")
        vlay2.addWidget(output)

        hlay2 = QHBoxLayout()
        vlay2.addLayout(hlay2)

        btn_lay = QVBoxLayout()
        hlay2.addLayout(btn_lay)

        cal = QPushButton("Calculate")
        cal.clicked.connect(self.cal)
        btn_lay.addWidget(cal)

        save = QPushButton("Save")
        save.clicked.connect(self.langefor_data)
        save.clicked.connect(self.pop_save)
        btn_lay.addWidget(save)

        cls = QPushButton("Clear")
        cls.clicked.connect(self.cls)
        btn_lay.addWidget(cls)

        border_left = BorderedFrame()
        hlay2.addWidget(border_left)

        bl_form = QFormLayout(border_left)
        bl_form.setLabelAlignment(Qt.AlignRight)

        self.volume_text = UnitLineEdit("m3", 20)
        bl_form.addRow("Volume of rock: ", self.volume_text)

        self.ton_text = UnitLineEdit("ton", 20)
        bl_form.addRow("Total rock broken: ", self.ton_text)

        self.exp_text = UnitLineEdit("kg", 16)
        bl_form.addRow("Total explosive used: ", self.exp_text)

        self.pf_text = UnitLineEdit("kg/m3", 25)
        bl_form.addRow("Powder Factor: ", self.pf_text)

        border_right = BorderedFrame()
        hlay2.addWidget(border_right)

        self.bot_charge = QLabel("Bottom charge")
        self.col_charge = QLabel("Column charge")
        self.lcc = QLabel("LCC")
        self.lcc_b = QLineEdit()
        self.lcc_c = QLineEdit()
        self.lcc_unit = QLabel("kg/m")
        self.cart_hole = QLabel("Total cartridges")
        self.cart_box = QLabel("Total box")
        self.cart_hole_bt = QLineEdit()
        self.cart_hole_ct = QLineEdit()
        self.cart_box_bt = QLineEdit()
        self.cart_box_ct = QLineEdit()

        grid3 = QGridLayout(border_right)
        grid3.addWidget(self.bot_charge, 0, 1)
        grid3.addWidget(self.col_charge, 0, 2)
        grid3.addWidget(self.lcc, 1, 0)
        grid3.addWidget(self.lcc_b, 1, 1)
        grid3.addWidget(self.lcc_c, 1, 2)
        grid3.addWidget(self.lcc_unit, 1, 3)
        grid3.addWidget(self.cart_hole, 2, 0)
        grid3.addWidget(self.cart_box, 3, 0)
        grid3.addWidget(self.cart_hole_bt, 2, 1)
        grid3.addWidget(self.cart_hole_ct, 2, 2)
        grid3.addWidget(self.cart_box_bt, 3, 1)
        grid3.addWidget(self.cart_box_ct, 3, 2)

    def cal(self):
        no = self.n_text.text()
        try:
            no_float = float(no)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')

        d1 = float(self.diameter_b.text())
        try:
            d1_float = float(d1)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        d2 = float(self.diameter_c.text())
        try:
            d2_float = float(d2)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        den1 = float(self.density_b.text())
        try:
            den1_float = float(den1)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
       
        den2 = float(self.density_c.text())
        try:
            den2_float = float(den2)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        
        area1 = (math.pi * d1 * d1) / 4
        try:
            area1_float = float(area1)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        area2 = (math.pi * d2 * d2) / 4
        try:
            area2_float = float(area2)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        cart1 = float(self.cart_hole_b.text())
        try:
            cart1_float = float(cart1)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        cart2 = float(self.cart_hole_c.text())
        try:
            cart2_float = float(cart2)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        box1 = int(self.cart_box_b.text())
        try:
            box1_float = float(box1)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        box2 = int(self.cart_box_c.text())
        try:
            box2_float = float(box2)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        xrow = float(self.row_text.text())
        try:
            xrow_float = float(xrow)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        s = float(self.spacing_text.value())
        try:
            s_float = float(s)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        b = float(self.burden_text.value())
        try:
            b_float = float(b)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        hole_depth = float(self.hole_depth_text.value())
        try:
            hole_depth_float = float(hole_depth)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        rock_den = float(self.rock_density_text.value())
        try:
            rock_den_float = float(rock_den)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        weight1 = float(self.weight_b.text())
        try:
            weight1_float = float(weight1)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')
        weight2 = float(self.weight_c.text())
        try:
            weight2_float = float(weight2)
        except ValueError:
            return QMessageBox.critical(self, 'Invalid Input', 'necessary parameters missing...')

        # linear charge
        lcc1 = area1 * den1 * 1000
        lcc2 = area2 * den2 *1000
        # Total cartridge
        Totalcart_1 = cart1 * float(no)
        Totalcart_2 = cart2 * float(no)

        # No of box used
        Totalbox1 = (Totalcart_1) / box1
        Totalbox2 = (Totalcart_2) / box2
        # Volume blasted
        xcolumn = float(no) / xrow
        x = (xrow - 1) / s
        y = (xcolumn - 1) / b
        Total_volume = hole_depth * x * y * float(no)
        # Tonnage
        Tonnage = Total_volume * rock_den
        # Total explosive used
        Total_explosive = (weight1 + weight2) * float(no)

        # Powder factor
        Powder_factor = Total_explosive / Total_volume

        self.lcc_b.setText(f"{lcc1: .1f}")
        self.lcc_c.setText(f"{lcc2: .1f}")
        self.cart_hole_bt.setText(f"{Totalcart_1: .1f}")
        self.cart_hole_ct.setText(f"{Totalcart_2: .1f}")
        self.cart_box_bt.setText(f"{Totalbox1: .1f}")
        self.cart_box_ct.setText(f"{Totalbox2: .1f}")
        self.volume_text.setValue(f"{Total_volume: .2f}")
        self.ton_text.setValue(f"{Tonnage: .2f}")
        self.exp_text.setValue(f"{Total_explosive: .2f}")
        self.pf_text.setValue(f"{Powder_factor: .2f}")
        ...

    def langefor_data(self):
        no = float(self.row_text.text())
        d1 = float(self.diameter_b.text())
        d2 = float(self.diameter_c.text())
        den1 = float(self.density_b.text())
        den2 = float(self.density_c.text())
        area1 = (math.pi * d1 * d1) / 4
        area2 = (math.pi * d2 * d2) / 4
        cart1 = float(self.cart_hole_b.text())
        cart2 = float(self.cart_hole_c.text())
        box1 = float(self.cart_box_b.text())
        box2 = float(self.cart_box_c.text())
        xrow = float(self.row_text.text())
        s = float(self.spacing_text.value())
        b = float(self.burden_text.value())
        hole_depth = float(self.hole_depth_text.value())
        density = float(self.rock_density_text.value())
        weight1 = float(self.weight_b.text())
        weight2 = float(self.weight_c.text())

        # linear charge
        lcc1 = area1 * den1 * 1000
        lcc2 = area2 * den2 * 1000
        # Total cartridge
        Totalcart_1 = cart1 * float(no)
        Totalcart_2 = cart2 * float(no)

        # No of box used
        Totalbox1 = (Totalcart_1 * float(no)) / box1
        Totalbox2 = (Totalcart_2 * float(no)) / box2
        # Volume blasted
        xcolumn = float(no) / xrow
        x = float((xrow - 1) / s)
        y = float((xcolumn - 1) / b)
        Total_volume = hole_depth * x * y * float(no)
        # Tonnage
        Tonnage = Total_volume * density
        # Total explosive used
        Total_explosive = (weight1 + weight2) * float(no)

        # Powder factor
        Powder_factor = (weight1 + weight2) / (hole_depth * s * b)

       
        ucs = self.ucs_text.value()
        rqd = self.rqd_text.value()
        density = float(self.rock_density_text.value())
        no_of_holes = int(self.n_text.text())
        Holes_per_row = int(self.row_text.text())
        subdrill = self.subdrill_text.value()
        hole_depth = float(self.hole_depth_text.value())
        stemming = self.stemming_text.value()
        fault_drill = self.fault_text.text()
        burden = float(self.burden_text.value())
        spacing = float(self.spacing_text.value())
        hole_diameter = self.hole_diameter_text.value()
        bench_height = self.bench_height_text.value()
        bottom_type = self.type_b.text()
        bottom_density = self.density_b.text()
        bottom_height = self.height_b.text()
        bottom_weight = float(self.weight_b.text())
        bottom_diameter = self.diameter_b.text()
        column_type = self.type_c.text()
        column_density = self.density_c.text()
        column_height = self.height_c.text()
        column_weight = float(self.weight_c.text())
        column_diameter = self.diameter_c.text()
        bottom_cartridge_per_hole = self.cart_hole_b.text()
        column_cartridge_per_hole = self.cart_hole_c.text()
        bottom_cartridge_per_box = self.cart_box_b.text()
        column_cartridge_per_box = self.cart_box_c.text()

        den1 = float(self.density_b.text())
        den2 = float(self.density_c.text())
        area1 = (math.pi * d1 * d1) / 4
        area2 = (math.pi * d2 * d2) / 4
        # linear charge
        lcc1 = area1 * den1 * 1000
        bottom_linear_charge = lcc1
        lcc2 = area2 * den2 * 1000
        column_linear_charge = lcc2
        # Total cartridge
        Totalcart_1 = cart1 * float(no)
        Total_cartridge_in_bottom = Totalcart_1
        Totalcart_2 = cart2 * float(no)
        Total_cartridge_in_column = Totalcart_2

        # No of box used
        Totalbox1 = (Totalcart_1 * float(no)) / box1
        Total_box_used_in_bottom_charge = Totalbox1

        Totalbox2 = (Totalcart_2 * float(no)) / box2
        Totak_box_used_in_column_charge = Totalbox2
        # Volume blasted
        #xcolumn = no / xrow
        #x = float((xrow - 1) * s)
        #y = float((xcolumn - 1) * b)
        Total_volume = float(hole_depth * spacing * burden * no_of_holes)
        # Tonnage
        Tonnage = float(Total_volume * density)
        # Total explosive used
        Total_explosive = float((weight1 + weight2) * no_of_holes)

        # Powder factor
        Powder_factor = float((weight1 + weight2) / (hole_depth * spacing * burden))

        query = "insert into manual_input(ucs,rqd,density,hole_diameter,bench_height,no_of_holes,Holes_per_row,subdrill,hole_depth,stemming,fault_drill,burden,spacing," \
                "bottom_type,column_type,bottom_height,column_height,bottom_weight,column_weight,bottom_density,column_density,bottom_diameter,column_diameter," \
                "bottom_cartridge_per_hole,column_cartridge_per_hole,bottom_cartridge_per_box,column_cartridge_per_box,Total_cartridge_in_bottom,Total_cartridge_in_column," \
                "Total_volume,Tonnage,Total_explosive,Powder_factor) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        value = (ucs,rqd,density,hole_diameter,bench_height,      no_of_holes,Holes_per_row,subdrill,hole_depth,stemming,fault_drill,burden,spacing,bottom_type,column_type,bottom_height,column_height,bottom_weight
        ,column_weight,bottom_density,column_density,bottom_diameter,column_diameter,bottom_cartridge_per_hole,column_cartridge_per_hole,bottom_cartridge_per_box
        ,column_cartridge_per_box,Total_cartridge_in_bottom,Total_cartridge_in_column,Total_volume,Tonnage,Total_explosive,Powder_factor)
        mydb.execute(query, value)
        mydb.commit()

   
    def pop_save(self):
        txt = QMessageBox()
        txt.setText("saved successfully")
        txt.setStandardButtons(QMessageBox.Ok)
        x = txt.exec_()
        ...

    def cls(self):
        return (self.ucs_text.cls(" ")
                ,self.rqd_text.cls(" ")
                ,self.rock_density_text.cls(" ")
                ,self.hole_diameter_text.cls(" ")
                ,self.bench_height_text.cls(" ")
                ,self.n_text.setText(" ")
                ,self.row_text.setText(" ")
                ,self.subdrill_text.cls(" ")
                ,self.hole_depth_text.cls(" ")
                ,self.stemming_text.cls(" ")
                ,self.fault_text.setText(" ")
                ,self.burden_text.cls(" ")
                ,self.spacing_text.cls(" ")
                ,self.type_b.setText(" ")
                ,self.height_b.setText(" ")
                ,self.weight_b.setText(" ")
                ,self.density_b.setText(" ")
                ,self.diameter_b.setText(" ")
                ,self.cart_hole_b.setText(" ")
                ,self.cart_box_b.setText(" ")
                , self.type_c.setText(" ")
                , self.height_c.setText(" ")
                , self.weight_c.setText(" ")
                , self.density_c.setText(" ")
                , self.diameter_c.setText(" ")
                , self.cart_hole_c.setText(" ")
                , self.cart_box_c.setText(" ")
                ,self.lcc_b.setText(" ")
                ,self.cart_hole_bt.setText(" ")
                ,self.cart_box_bt.setText(" ")
                , self.lcc_c.setText(" ")
                , self.cart_hole_ct.setText(" ")
                , self.cart_box_ct.setText(" ")
                ,self.volume_text.cls(" ")
                ,self.ton_text.cls(" ")
                ,self.exp_text.cls(" ")
                ,self.pf_text.cls(" "))
        ...
