from qt import *
from optimiser import *

class LKOptimiser(BorderedFrame):
    def __init__(self, input):
        super().__init__()

        self.input = input
        lay = QVBoxLayout(self)

        button = QPushButton("L-k Optimiser")
        button.clicked.connect(self.send_values)
        lay.addWidget(button)

        cont = QFrame()
        lay.addWidget(cont)

        form = QFormLayout(cont)
        form.setContentsMargins(0, 0, 0, 0)

        self.ucs = UnitLineEdit("Mpa", 25)
        form.addRow("UCS: ", self.ucs)

        self.rqd = UnitLineEdit("%")
        form.addRow("RQD: ", self.rqd)

        self.rock_density = UnitLineEdit("g/cc", 205)
        form.addRow("Rock Density: ", self.rock_density)

        self.bi = LineEdit()
        form.addRow("BI: ", self.bi)
    
    def send_values(self):
        self.input.optimise(
            ucs=self.ucs.value(),
            rqd=self.rqd.value(),
            rock_density=self.rock_density.value(),
            bi=self.bi.value(),
        )


class Input(QFrame):
    def __init__(self, home: "Home"):
        super().__init__()
        self.home = home
        lay = QVBoxLayout(self)
        lay.setSpacing(2)

        title_frame = QFrame()
        lay.addWidget(title_frame)

        title_lay = QHBoxLayout(title_frame)
        title_lay.setContentsMargins(0, 0, 0, 0)

        icon = Icon("download")
        title_lay.addWidget(icon)

        label = CenterLabel("Input : ")
        title_lay.addWidget(label)

        title_lay.addStretch()

        hlay = QHBoxLayout()
        lay.addLayout(hlay)

        frame_1 = BorderedFrame()
        hlay.addWidget(frame_1)
        form_1 = QFormLayout(frame_1)

        self.hole_diameter = UnitLineEdit('mm', 25)
        form_1.addRow("Hole diameter: ", self.hole_diameter)

        self.bench_height = MeterEdit()
        form_1.addRow("Bench height: ", self.bench_height)

        x_lay = QHBoxLayout()
        self.width1 = LineEdit()
        x_lay.addWidget(self.width1)

        x_lay.addWidget(QLabel("x"))

        self.width2 = MeterEdit()
        x_lay.addLayout(self.width2)

        form_1.addRow("Width of round: ", x_lay)

        self.optimiser = LKOptimiser(self)
        hlay.addWidget(self.optimiser)

        frame_2 = BorderedFrame()
        frame_2.setObjectName("frame_2")
        hlay.addWidget(frame_2)
        grid_lay = QGridLayout(frame_2)
        grid_lay.setSpacing(2)
        # grid_lay.setContentsMargins(0, 0, 0, 0)

        grid_lay.addWidget(QLabel(""), 0, 0)
        grid_lay.addWidget(QLabel("Bottom charge"), 0, 1)
        grid_lay.addWidget(QLabel("Column charge"), 0, 2)
        grid_lay.addWidget(QLabel("Type"), 1, 0)
        grid_lay.addWidget(QLabel("Density"), 2, 0)

        self.type_bottom_charge = LineEdit()
        grid_lay.addWidget(self.type_bottom_charge, 1, 1)

        self.type_column_charge = LineEdit()
        grid_lay.addWidget(self.type_column_charge, 1, 2)

        self.density_bottom_charge = LineEdit()
        grid_lay.addWidget(self.density_bottom_charge, 2, 1)

        self.density_column_charge = UnitLineEdit("g/cc", 25)
        grid_lay.addLayout(self.density_column_charge, 2, 2)

        self.setStyleSheet("QFrame#frame_2 QLabel{max-height: 20px}")

    def optimise(self, **kwargs):
        hole_diameter = self.hole_diameter.value()
        bench_height = self.bench_height.value()
        ucs = kwargs['ucs']
        rqd = kwargs['rqd']
        bi = kwargs['bi']

        invalid_value = ''
        if not hole_diameter:
            invalid_value = 'Hole Diameter'
        elif not bench_height:
            invalid_value = "Bench Height"
        elif not ucs:
            invalid_value = "UCS"
        elif not rqd:
            invalid_value = "RQD"
        elif not bi:
            invalid_value = "BI"

        if invalid_value:
            return QMessageBox.critical(self, 'Invalid Inputs', f"{invalid_value} can not be zero")

        values = [hole_diameter, bench_height, ucs, rqd, bi]
        result = predict_blast(values)
        print(result)
        powder_factor = (f"{result[0][0]: .4f}")
        self.home.output.powder_factor.setValue(powder_factor)
  

       

class Output(QFrame):
    def __init__(self, home: "Home"):
        super().__init__()
        self.home = home
        lay = QVBoxLayout(self)
        lay.setSpacing(2)

        title_frame = QFrame()
        lay.addWidget(title_frame)

        title_lay = QHBoxLayout(title_frame)
        title_lay.setContentsMargins(0, 0, 0, 0)

        icon = Icon("upload")
        title_lay.addWidget(icon)

        label = CenterLabel("Output : ")
        title_lay.addWidget(label)

        title_lay.addStretch()

        hlay = QHBoxLayout()
        lay.addLayout(hlay)

        frame_1 = BorderedFrame()
        hlay.addWidget(frame_1)
        form_1 = QFormLayout(frame_1)

        self.maximum_burden = O_MeterEdit()
        form_1.addRow("Maximum Burden: ", self.maximum_burden)

        self.sub_drill = O_MeterEdit()
        form_1.addRow("Sub-drill: ", self.sub_drill)

        self.hole_depth = O_MeterEdit()
        form_1.addRow("Hole-depth: ", self.hole_depth)

        self.stemming = O_MeterEdit()
        form_1.addRow("Stemming: ", self.stemming)

        self.faulty = O_LineEdit()
        form_1.addRow("Faulty D-H factor: ", self.faulty)

        self.burden = O_MeterEdit()
        form_1.addRow("Burden: ", self.burden)

        self.spacing = O_MeterEdit()
        form_1.addRow("Spacing: ", self.spacing)

        self.no_of_holes = O_LineEdit()
        form_1.addRow("No-of-holes: ", self.no_of_holes)

        self.volume = O_UnitLineEdit("m3")
        self.volume.unit.setText(
            '<html><head/><body><p>m<span style=" font-size:10pt; vertical-align:super;">3</span></p></body></html>'
        )
        form_1.addRow("Volume of blast rock: ", self.volume)

        hlay.addSpacing(40)

        vlay = QVBoxLayout()
        hlay.addLayout(vlay)

        frame_2 = BorderedFrame()
        vlay.addWidget(frame_2)

        grid_lay = QGridLayout(frame_2)

        grid_lay.addWidget(QLabel(""), 0, 0)
        grid_lay.addWidget(QLabel("Bottom charge"), 0, 1)
        grid_lay.addWidget(QLabel("Column charge"), 0, 2)

        grid_lay.addWidget(QLabel("Linear charge conc"), 1, 0)
        grid_lay.addWidget(QLabel("Height"), 2, 0)
        grid_lay.addWidget(QLabel("Weight"), 3, 0)

        self.linear_bottom_charge = O_LineEdit()
        grid_lay.addWidget(self.linear_bottom_charge, 1, 1)

        self.linear_column_charge = O_UnitLineEdit("kg/m", 27)
        grid_lay.addLayout(self.linear_column_charge, 1, 2)

        self.linear_column_charge_percent = QSpinBox()
        self.linear_column_charge_percent.setMinimum(40)
        self.linear_column_charge_percent.setMaximum(50)
        self.linear_column_charge_percent.valueChanged.connect(self.percent_changed)
        grid_lay.addWidget(self.linear_column_charge_percent, 1, 3)

        self.height_bottom_charge = O_LineEdit()
        grid_lay.addWidget(self.height_bottom_charge, 2, 1)

        self.height_column_charge = O_MeterEdit()
        grid_lay.addLayout(self.height_column_charge, 2, 2)

        self.weight_bottom_charge = O_LineEdit()
        grid_lay.addWidget(self.weight_bottom_charge, 3, 1)

        self.weight_column_charge = O_UnitLineEdit("kg", 20)
        grid_lay.addLayout(self.weight_column_charge, 3, 2)

        self.setStyleSheet("QFrame#frame_2 QLabel{max-height: 20px; background: red}")

        vlay.addStretch()

        vlay.addSpacing(10)

        bottom_frame = BorderedFrame()
        vlay.addWidget(bottom_frame)

        form_2 = QFormLayout(bottom_frame)

        self.total_explosive = UnitLineEdit("kg", 10)
        form_2.addRow("Total Explosive used", self.total_explosive)

        self.powder_factor = UnitLineEdit(
            '<html><head/><body><p>kg/m<span style=" font-size:10pt; vertical-align:super;">3</span></p></body></html>',
            35,
        )
        form_2.addRow("Powder Factor", self.powder_factor)

        vlay.addStretch()
        hlay.addStretch()

        button_lay = QHBoxLayout()
        vlay.addLayout(button_lay)

        calculate = IconButton("Calculate", "equal")
        calculate.clicked.connect(self.calculate)
        button_lay.addWidget(calculate)

        save = IconButton("Save", "save")
        save.clicked.connect(self.langefor_data)
        save.clicked.connect(self.pop_save)
        button_lay.addWidget(save)
        button_lay.addWidget(save)

        reset = IconButton("Reset", "clear")
        reset.clicked.connect(self.reset)
        button_lay.addWidget(reset)

        vlay.addStretch()
        self.w = QColorDialog()
        self.w.currentColorChanged.connect(lambda c: print(c.name()))
    
    def percent_changed(self, value):
        value /= 100
        Qbk = self.linear_bottom_charge.value()
        Qpk = Qbk * value
        self.linear_column_charge.setValue(Qpk)

        hp = self.height_column_charge.value()
        Qp = Qpk * hp
        self.weight_column_charge.setValue(Qp)

        Qt = Qp + self.weight_bottom_charge.value()
        T = Qt * self.no_of_holes.value()
        self.total_explosive.setValue(T)

        V = self.burden.value()
        E1 = self.spacing.value()
        K = self.home.input.bench_height.value()

        PF =  Qt /  (V * E1 * K)
        self.powder_factor.setValue_PF(PF)


    def calculate(self):
        input = self.home.input

        #parameters
        D = input.hole_diameter.value()
        if not D:
            return QMessageBox.critical(self, 'Invalid Input', 'Hole Diameter can not be zero')
        
        Vmax = 45 * (D/1000)
        self.maximum_burden.setValue(Vmax)

        K = input.bench_height.value()
        if not K:
            return QMessageBox.critical(self, 'Invalid Input', 'Bench Height can not be zero')
        B1 = input.width1.value()
        if not B1:
            return QMessageBox.critical(self, 'Invalid Input', 'Input span of round')
        B2 = input.width2.value()
        if not B2:
            return QMessageBox.critical(self, 'Invalid Input', 'Input span of round')
        U = 0.3 * Vmax
        self.sub_drill.setValue(U)

        H = (K+U) * 1.05
        self.hole_depth.setValue(H)

        F = 0.05 + (0.03 *H)
        self.faulty.setValue(F)

        V = Vmax - F
        self.burden.setValue(V)

        ho = V
        self.stemming.setValue(ho)

        # proposed burden per column
        b = round(B2 / V)
        # No of holes per column
        y = b + 1
        E1 = 1.25 * V
         # Proposed spacing per row
        s = round(B1 / E1)
        # No of holes per row
        x = s + 1
        # Actual Spacing
        E = B1 / s
        self.spacing.setValue(E)

        # Area of blasted area
        Area = B1 * B2
        # Volume of balsted rock
        Vol = H * Area
        self.volume.setValue(Vol)

        # Total No of holes
        n = x * y
        self.no_of_holes.setValue(n)

        # Explosive Parameter
        # Calculation for Bottom Charge
        # Linear charge concentration
        Qbk = ((D * D) / 1000)
        self.linear_bottom_charge.setValue(Qbk)

        # Calculation for Column Charge
        # Linear charge concentration
        Qpk = .4 * Qbk
        self.linear_column_charge.setValue(Qpk)
        # Height of bottom charge
        hb = 1.3 * Vmax
        self.height_bottom_charge.setValue(hb)
        # Quantity of Bottom charge
        Qb = hb * Qbk
        self.weight_bottom_charge.setValue(Qb)
        # Height of Column charge
        hp = H - (hb + ho)
        self.height_column_charge.setValue(hp)
        Qp = hp * Qpk
        self.weight_column_charge.setValue(Qp)
        Qt = Qp + self.weight_bottom_charge.value()
        T = Qt * self.no_of_holes.value()
        self.total_explosive.setValue(T)
        PF =  Qt /  (V * E1 * K)
        self.powder_factor.setValue_PF(PF)

        self.linear_column_charge_percent.setValue(40)

    def langefor_data(self):
        input = self.home.input
        output = self.home.output
        #parameters
        D = float(input.hole_diameter.value())
        Vmax = 45 * (D/1000)
        K = float(input.bench_height.value())
        B1 = float(input.width1.value())
        B2 = float(input.width2.value())
        U = 0.3 * Vmax
        H = (K+U) * 1.05
        F = 0.05 + (0.03 * H)
        V = Vmax - F
        ho = V
        # proposed burden per column
        b = round(B2 / V)
        # No of holes per column
        y = b + 1
        #calculating spacing
        E1 = 1.25 * V
         # Proposed spacing per row
        s = round(B1 / E1)
        # No of holes per row
        x = s + 1
        # Actual Spacing
        E = round((B1 / s), 2)
        # Area of blasted area
        Area = B1 * B2
        # Volume of balsted rock
        Vol = H * Area
       
        # Total No of holes
        n = x * y

        # Explosive Parameter
        # Calculation for Bottom Charge
        # Linear charge concentration
        Qbk = ((D * D) / 1000)

        # Calculation for Column Charge
        # Linear charge concentration
        Qpk = .4 * Qbk
       
        # Height of bottom charge
        hb = 1.3 * Vmax
       
        # Quantity of Bottom charge
        Qb = hb * Qbk
       
        # Height of Column charge
        hp = H - (hb + ho)
        
        Qp = hp * Qpk
       
        Qt = Qp + self.weight_bottom_charge.value()
        T = Qt * self.no_of_holes.value()
       
        PF =  Qt /  (V * E1 * K)

        hole_diameter = input.hole_diameter.value()
        bench_height = input.bench_height.value()
        width = input.width1. value()
        length = input.width1.value()
        ucs = input.optimiser.ucs.value()
        rqd = input.optimiser.rqd.value()
        rock_density = input.optimiser.rock_density.value()
        bi = input.optimiser.bi.value()
        bottom_type = input.type_bottom_charge.text()
        bottom_density = input.density_bottom_charge.value()
        column_type = input.type_column_charge.text()
        column_density = input.density_column_charge.value()
        maximum_burden = Vmax
        subdrill = U
        burden = V
        spacing = E
        hole_depth = H
        fault_drill = F
        no_of_holes = output.no_of_holes.value()
        stemming = ho
        bottom_linear_charge = Qbk
        bottom_height = hb
        bottom_weight = Qb
        column_linear_charge = Qpk
        column_weight = Qp
        column_height = hp
        volume_blasted = Vol
        powder_factor = PF
        total_explosive_used = T

        query = "INSERT INTO langefor (hole_diameter, bench_height, width, length, ucs, rqd, rock_density, bi, bottom_type, bottom_density, column_type, column_density, maximum_burden, subdrill, burden, spacing, hole_depth, fault_drill, no_of_holes, stemming, bottom_linear_charge, bottom_height, bottom_weight, column_linear_charge, column_weight, column_height, volume_blasted, powder_factor, total_explosive_used) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

        value = (hole_diameter, bench_height, width, length, ucs, rqd, rock_density, bi, bottom_type, bottom_density, column_type, column_density, maximum_burden, subdrill, burden, spacing, hole_depth, fault_drill, no_of_holes, stemming, bottom_linear_charge, bottom_height, bottom_weight, column_linear_charge, column_weight, column_height, volume_blasted, powder_factor, total_explosive_used)

        mydb.execute(query, value)
        mydb.commit()


    def reset(self):
        clear1 = self.home.input
        clear2 = self.home.output
        return (clear1.optimiser.ucs.cls(),
                clear1.optimiser.rqd.cls(),
                clear1.optimiser.bi.clx(),
                clear1.optimiser.rock_density.cls(),
                clear1.hole_diameter.cls(),
                clear1.bench_height.cls(),
                clear1.width1.clx(),
                clear1.width2.cls(),
                clear1.type_bottom_charge.clx(),
                clear1.type_column_charge.clx(),
                clear1.density_bottom_charge.clx(),
                clear1.density_column_charge.cls(),
                clear2.maximum_burden.cls(),
                clear2.sub_drill.cls(),
                clear2.hole_depth.cls(),
                clear2.stemming.cls(),
                clear2.faulty.clx(),
                clear2.burden.cls(),
                clear2.spacing.cls(),
                clear2.no_of_holes.clx(),
                clear2.volume.cls(),
                clear2.linear_bottom_charge.clx(),
                clear2.linear_column_charge.cls(),
                clear2.height_bottom_charge.clx(),
                clear2.height_column_charge.cls(),
                clear2.weight_bottom_charge.clx(),
                clear2.weight_column_charge.cls(),
                clear2.total_explosive.cls(),
                clear2.powder_factor.cls()
                )
    
    def pop_save(self):
        txt = QMessageBox()
        txt.setText("saved successfully")
        txt.setStandardButtons(QMessageBox.Ok)
        x = txt.exec_()


class Home(BaseWindow):
    def __init__(self, *args):
        super().__init__(*args)

        lay = QVBoxLayout(self)

        hlay = QHBoxLayout()
        lay.addLayout(hlay)

        hlay.addWidget(self.menu)

        blast_design = CenterLabel("Blast Design")
        hlay.addWidget(blast_design)

        self.input = Input(self)
        lay.addWidget(self.input)

        self.output = Output(self)
        lay.addWidget(self.output)

        lay.addStretch()
